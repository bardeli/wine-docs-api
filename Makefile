help: # List all available commands
	@echo "Available commands:"
	@echo ""
	@cat Makefile | grep -E '^[a-zA-Z0-9_-]+:.*?# .*$$' | awk 'BEGIN {FS = ":.*?# "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

install: # Install dependencies
	@pipenv install

docker-run: # Run the application
	@docker build -t wine-docs-api . && docker run -p 8000:80 wine-docs-api

test: # Run tests
	@pipenv run coverage run --omit='*/__init__.py,*/test_*.py' -m pytest -v && pipenv run coverage report

test-html:
	@pipenv run coverage run --omit='*/__init__.py,*/test_*.py' -m pytest -v && pipenv run coverage html

requirements: # Generate requirements.txt
	@pipenv requirements > requirements.txt

lint: # Run linter
	@pipenv run black -l 120 src && pipenv run flake8 -v

format: # Format code
	@pipenv run black -l 120 src
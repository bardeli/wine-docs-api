from unittest.mock import patch

from fastapi.testclient import TestClient

from src.application.api.grapes_router import get_production_use_case, oauth2_scheme
from src.main import app

client = TestClient(app)


def get_test_user():
    return "test_user"


app.dependency_overrides[oauth2_scheme] = get_test_user


@patch.object(get_production_use_case, "execute")
def test_get_productions_successful(mock_execute):
    mock_execute.return_value = {"year": "2022", "production": 1000}
    response = client.get("/grapes/farming/2022")
    assert response.status_code == 200
    assert response.json() == {"year": "2022", "production": 1000}


@patch.object(get_production_use_case, "execute")
def test_get_productions_not_found(mock_execute):
    mock_execute.return_value = None
    response = client.get("/grapes/farming/2022")
    assert response.status_code == 404


@patch.object(get_production_use_case, "execute")
def test_get_productions_error_occurred(mock_execute):
    mock_execute.side_effect = Exception("Error occurred")
    response = client.get("/grapes/farming/2022")
    assert response.status_code == 400
    assert response.json() == {"detail": "Error occurred"}

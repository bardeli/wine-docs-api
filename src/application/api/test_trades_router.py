import pytest
from fastapi.testclient import TestClient

from src.application.api.trades_router import oauth2_scheme
from src.main import app

client = TestClient(app)


def get_test_user():
    return "test_user"


app.dependency_overrides[oauth2_scheme] = get_test_user


@pytest.mark.parametrize(
    "mock_execute_return, status_code, json_response",
    [
        ({"year": "2022", "exportation": 1000}, 200, {"year": "2022", "exportation": 1000}),
        (None, 404, {"detail": "Not Found"}),
        (Exception("Error occurred"), 400, {"detail": "Error occurred"}),
    ],
    ids=["successful_exportation", "exportation_not_found", "exportation_error_occurred"],
)
def test_get_exportation(mock_execute_return, status_code, json_response, mocker):
    mock_execute = mocker.patch("src.application.api.trades_router.get_trades_use_case.execute")
    if isinstance(mock_execute_return, Exception):
        mock_execute.side_effect = mock_execute_return
    else:
        mock_execute.return_value = mock_execute_return
    response = client.get("/trades/exportation/2022")
    assert response.status_code == status_code
    assert response.json() == json_response


@pytest.mark.parametrize(
    "mock_execute_return, status_code, json_response",
    [
        ({"year": "2022", "importation": 1000}, 200, {"year": "2022", "importation": 1000}),
        (None, 404, {"detail": "Not Found"}),
        (Exception("Error occurred"), 400, {"detail": "Error occurred"}),
    ],
    ids=["successful_importation", "importation_not_found", "importation_error_occurred"],
)
def test_get_importation(mock_execute_return, status_code, json_response, mocker):
    mock_execute = mocker.patch("src.application.api.trades_router.get_trades_use_case.execute")
    if isinstance(mock_execute_return, Exception):
        mock_execute.side_effect = mock_execute_return
    else:
        mock_execute.return_value = mock_execute_return
    response = client.get("/trades/importation/2022")
    assert response.status_code == status_code
    assert response.json() == json_response

from unittest.mock import patch

import pytest
from fastapi import HTTPException
from fastapi.testclient import TestClient
from src.application.api.authentication_router import router, authenticate_use_case, get_user_use_case

client = TestClient(router)


@patch.object(authenticate_use_case, "execute")
def test_login_for_access_token_returns_token_when_credentials_are_valid(mock_execute):
    mock_execute.return_value = "test_token"
    response = client.post("/token", data={"username": "test_user", "password": "test_password"})
    assert response.status_code == 200
    assert response.json() == {"access_token": "test_token", "token_type": "bearer"}


@patch.object(authenticate_use_case, "execute")
def test_login_for_access_token_returns_401_when_credentials_are_invalid(mock_execute):
    mock_execute.return_value = None
    with pytest.raises(HTTPException) as exc_info:
        client.post("/token", data={"username": "test_user", "password": "wrong_password"})
    assert exc_info.value.status_code == 401


@patch.object(get_user_use_case, "execute")
def test_read_users_me_returns_user_when_token_is_valid(mock_execute):
    mock_execute.return_value = {
        "username": "test_user",
        "email": "test_user@example.com",
        "disabled": False,
        "full_name": "Test User",
    }
    response = client.get("/users/me", headers={"Authorization": "Bearer test_token"})
    assert response.status_code == 200
    assert response.json() == {
        "username": "test_user",
        "email": "test_user@example.com",
        "disabled": False,
        "full_name": "Test User",
    }


@patch.object(get_user_use_case, "execute")
def test_read_users_me_returns_401_when_token_is_invalid(mock_execute):
    mock_execute.return_value = None
    with pytest.raises(HTTPException) as exc_info:
        client.get("/users/me", headers={"Authorization": "Bearer wrong_token"})
    assert exc_info.value.status_code == 401

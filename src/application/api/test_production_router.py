from unittest.mock import patch

from fastapi.testclient import TestClient

from src.application.api.products_router import oauth2_scheme
from src.main import app

client = TestClient(app)


def get_test_user():
    return "test_user"


app.dependency_overrides[oauth2_scheme] = get_test_user


@patch("src.application.api.products_router.GetProduction.execute")
def test_get_productions_returns_production_when_year_exists(mock_execute):
    mock_execute.return_value = {"year": "2022", "production": 1000}
    response = client.get("/products/productions/2022")
    assert response.status_code == 200
    assert response.json() == {"year": "2022", "production": 1000}


@patch("src.application.api.products_router.GetProduction.execute")
def test_get_productions_returns_404_when_year_does_not_exist(mock_execute):
    mock_execute.return_value = None
    response = client.get("/products/productions/2022")
    assert response.status_code == 404


@patch("src.application.api.products_router.GetProduction.execute")
def test_get_productions_returns_400_when_exception_occurs(mock_execute):
    mock_execute.side_effect = Exception("Error occurred")
    response = client.get("/products/productions/2022")
    assert response.status_code == 400
    assert response.json() == {"detail": "Error occurred"}

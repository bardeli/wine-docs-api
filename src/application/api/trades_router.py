import logging
from typing import Annotated

from fastapi import HTTPException, Depends, APIRouter
from fastapi.security import OAuth2PasswordBearer

from src.domain.use_cases.get_trades_information import GetTradesInformation, TradeType
from src.infrastructure.repositories.trades_scrap_repository import TradesScrapRepository

router = APIRouter(prefix="/trades")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


# Instantiate the repository and use case
repo = TradesScrapRepository()
get_trades_use_case = GetTradesInformation(repo)


@router.get("/exportation/{year}")
async def get_exportation(year: str, token: Annotated[str, Depends(oauth2_scheme)]):
    logging.info(f"Year: {year}")
    try:
        production = get_trades_use_case.execute(TradeType.EXPORT, int(year))
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))

    if not production:
        raise HTTPException(status_code=404)

    return production


@router.get("/importation/{year}")
async def get_importation(year: str, token: Annotated[str, Depends(oauth2_scheme)]):
    logging.info(f"Year: {year}")
    try:
        production = get_trades_use_case.execute(TradeType.IMPORT, int(year))
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))

    if not production:
        raise HTTPException(status_code=404)

    return production

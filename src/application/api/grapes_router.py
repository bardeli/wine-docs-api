import logging
from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer

from src.domain.use_cases.get_grape_farming import GetGrapeFarming
from src.infrastructure.repositories.grape_scrap_repository import (
    GrapeFarmingScrapRepository,
)

router = APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


# Instantiate the repository and use case
repo = GrapeFarmingScrapRepository()
get_production_use_case = GetGrapeFarming(repo)


@router.get("/grapes/farming/{year}")
async def get_productions(year: int, token: Annotated[str, Depends(oauth2_scheme)]):
    logging.info(f"Year: {year}")
    try:
        production = get_production_use_case.execute(year)
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))

    if not production:
        raise HTTPException(status_code=404)

    return production

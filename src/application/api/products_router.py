import logging
from typing import Annotated

from fastapi import HTTPException, Depends, APIRouter
from fastapi.security import OAuth2PasswordBearer

from src.domain.use_cases.get_production import GetProduction
from src.domain.use_cases.get_sales import GetSales
from src.infrastructure.repositories.wine_production_scrap_repository import (
    WineProductionScrapRepository,
)
from src.infrastructure.repositories.wine_sales_scrap_repository import WineSalesScrapRepository

router = APIRouter(prefix="/products")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


# Instantiate the repository and use case
repo = WineProductionScrapRepository()
sales_repo = WineSalesScrapRepository()
get_production_use_case = GetProduction(repo)
get_sales_use_case = GetSales(sales_repo)


@router.get("/productions/{year}")
async def get_productions(year: str, token: Annotated[str, Depends(oauth2_scheme)]):
    logging.info(f"Year: {year}")
    try:
        production = get_production_use_case.execute(int(year))
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))

    if not production:
        raise HTTPException(status_code=404)

    return production


@router.get("/sales/{year}")
async def get_sales(year: str, token: Annotated[str, Depends(oauth2_scheme)]):
    logging.info(f"Year: {year}")
    try:
        production = get_sales_use_case.execute(int(year))
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))

    if not production:
        raise HTTPException(status_code=404)

    return production

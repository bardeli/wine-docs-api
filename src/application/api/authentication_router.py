from typing import Annotated

from fastapi import HTTPException, Depends, status, APIRouter
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from passlib.context import CryptContext

from src.domain.entities.token import Token
from src.domain.entities.user import User
from src.domain.use_cases.authenticate_user import AuthenticateUser
from src.domain.use_cases.get_authenticated_user import GetAuthenticatedUser
from src.infrastructure.repositories.user_memory_repository import UserMemoryRepository

router = APIRouter()


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
user_repo = UserMemoryRepository()
authenticate_use_case = AuthenticateUser(user_repo)
get_user_use_case = GetAuthenticatedUser(user_repo)


@router.post("/token")
async def login_for_access_token(form_data: Annotated[OAuth2PasswordRequestForm, Depends()]) -> Token:
    token = authenticate_use_case.execute(form_data.username, form_data.password)
    if not token:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    return Token(access_token=token, token_type="bearer")


@router.get("/users/me", response_model=User)
async def read_users_me(token: Annotated[str, Depends(oauth2_scheme)]):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    user = get_user_use_case.execute(token)

    if user is None:
        raise credentials_exception
    return user

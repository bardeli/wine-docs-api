from typing import Optional

from pydantic import BaseModel


class Product(BaseModel):
    item: str
    subitem: Optional[str]
    quantity: Optional[float]

    def __repr__(self):
        return f"{self.item} - {self.subitem} - {self.quantity}"

    def __eq__(self, other):
        return self.item == other.item and self.subitem == other.subitem and self.quantity == other.quantity

from pydantic import BaseModel


class Trade(BaseModel):
    country: str
    product: str
    quantity: float | None = None
    value: float | None = None

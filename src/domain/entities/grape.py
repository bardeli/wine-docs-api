from pydantic import BaseModel


class Grape(BaseModel):
    item: str
    subitem: str | None = None
    quantity: float | None = None
    classification: str | None = None

from abc import ABC, abstractmethod
from typing import List

from src.domain.entities.trade import Trade


class GrapeAndProductTradesRepository(ABC):
    @abstractmethod
    def get_imported_grape_derived_products(self, year: int) -> List[Trade]:
        pass

    @abstractmethod
    def get_exported_grape_derived_products(self, year: int) -> List[Trade]:
        pass

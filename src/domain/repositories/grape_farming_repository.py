from abc import ABC, abstractmethod
from typing import List

from src.domain.entities.grape import Grape


class GrapeFarmingRepository(ABC):

    @abstractmethod
    def get_grape_farming_by_year(self, year: int) -> List[Grape]:
        pass

    @abstractmethod
    def get_year_limits(self):
        pass

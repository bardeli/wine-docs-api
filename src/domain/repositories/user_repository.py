from abc import ABC, abstractmethod

from src.domain.entities.user import User


class UserRepository(ABC):

    @abstractmethod
    def get_user(self, username: str) -> User:
        pass

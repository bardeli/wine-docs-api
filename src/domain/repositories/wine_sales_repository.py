from abc import ABC, abstractmethod
from typing import List

from src.domain.entities.product import Product


class WineSalesRepository(ABC):
    @abstractmethod
    def get_sales(self, year: int) -> List[Product]:
        pass

    @abstractmethod
    def get_year_limits(self) -> tuple[int, int]:
        pass

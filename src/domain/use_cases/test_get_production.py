import unittest
from unittest.mock import Mock

from src.domain.use_cases.get_production import GetProduction


class TestGetProduction(unittest.TestCase):
    def setUp(self):
        self.repo = Mock()
        self.get_production = GetProduction(self.repo)

    def test_no_year_provided(self):
        with self.assertRaises(Exception):
            self.get_production.execute(None)

    def test_year_greater_than_current_year(self):
        with self.assertRaises(Exception):
            self.get_production.execute(3000)

    def test_year_outside_production_range(self):
        self.repo.get_year_limits.return_value = (2000, 2020)
        result = self.get_production.execute(1999)
        self.assertEqual(result, [])

    def test_year_within_production_range(self):
        self.repo.get_year_limits.return_value = (2000, 2020)
        self.repo.get_production.return_value = ["product1", "product2"]
        result = self.get_production.execute(2010)
        self.assertEqual(result, ["product1", "product2"])


if __name__ == "__main__":
    unittest.main()

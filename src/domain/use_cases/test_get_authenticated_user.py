from unittest.mock import Mock

import pytest
from jose import jwt

from src.domain.entities.user import AuthUser
from src.domain.use_cases.get_authenticated_user import GetAuthenticatedUser


@pytest.fixture
def mock_user_repository():
    return Mock()


@pytest.fixture
def auth_user(mock_user_repository):
    return GetAuthenticatedUser(user_repository=mock_user_repository)


@pytest.fixture
def token(auth_user):
    return jwt.encode({"sub": "test_user"}, auth_user.SECRET_KEY, algorithm=auth_user.ALGORITHM)


def test_authenticate_user_returns_token_when_credentials_are_valid(auth_user, mock_user_repository, token):
    mock_user_repository.get_user.return_value = AuthUser(
        username="test_user", hashed_password="test_password", disabled=False
    )
    user = auth_user.execute(token)
    assert user is not None
    assert user.username == "test_user"


def test_authenticate_user_returns_false_when_user_does_not_exist(mock_user_repository, auth_user, token):
    mock_user_repository.get_user.return_value = None
    result = auth_user.execute(token)
    assert result is None


def test_authenticate_user_returns_false_when_user_is_disabled(mock_user_repository, auth_user, token):
    mock_user_repository.get_user.return_value = AuthUser(
        username="test_user", hashed_password="test_password", disabled=True
    )
    result = auth_user.execute(token)
    assert result is None

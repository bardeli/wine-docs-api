from datetime import datetime

from src.domain.entities.grape import Grape
from src.domain.repositories.grape_farming_repository import GrapeFarmingRepository


class GetGrapeFarming:
    def __init__(self, grape_farming_repo: GrapeFarmingRepository):
        self.grape_farming_repo = grape_farming_repo

    def execute(self, year: int) -> list[Grape]:

        if not year:
            raise Exception("Year is required")

        current_year = datetime.now().year

        if int(year) > current_year:
            raise Exception("Invalid year")

        start, end = self.grape_farming_repo.get_year_limits()

        if year < start or year > end:
            return []

        return self.grape_farming_repo.get_grape_farming_by_year(year)

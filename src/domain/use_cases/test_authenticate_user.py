from unittest.mock import patch

from src.domain.entities.user import AuthUser
from src.domain.repositories.user_repository import UserRepository
from src.domain.use_cases.authenticate_user import AuthenticateUser


@patch.object(UserRepository, "get_user")
def test_authenticate_user_returns_token_when_credentials_are_valid(mock_get_user):
    mock_get_user.get_user.return_value = AuthUser(
        username="test_user", hashed_password=AuthenticateUser.pwd_context.hash("secret"), disabled=False
    )
    auth_user = AuthenticateUser(user_repository=mock_get_user)
    token = auth_user.execute(username="test_user", password="secret")
    assert token is not False


@patch.object(UserRepository, "get_user")
def test_authenticate_user_returns_false_when_user_does_not_exist(mock_get_user):
    mock_get_user.return_value = None
    auth_user = AuthenticateUser(user_repository=mock_get_user)
    result = auth_user.execute(username="test_user", password="test_password")
    assert result is False


@patch.object(UserRepository, "get_user")
def test_authenticate_user_returns_false_when_user_is_disabled(mock_get_user):
    mock_get_user.return_value = AuthUser(username="test_user", hashed_password="test_password", disabled=True)
    auth_user = AuthenticateUser(user_repository=mock_get_user)
    result = auth_user.execute(username="test_user", password="test_password")
    assert result is False


@patch.object(UserRepository, "get_user")
def test_authenticate_user_returns_false_when_password_is_incorrect(mock_get_user):
    mock_get_user.return_value = AuthUser(
        username="test_user", hashed_password=AuthenticateUser.pwd_context.hash("secret"), disabled=False
    )
    auth_user = AuthenticateUser(user_repository=mock_get_user)
    result = auth_user.execute(username="test_user", password="wrong_password")
    assert result is False

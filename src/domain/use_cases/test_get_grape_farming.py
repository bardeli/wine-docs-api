from unittest.mock import Mock

import pytest

from src.domain.entities.grape import Grape
from src.domain.repositories.grape_farming_repository import GrapeFarmingRepository
from src.domain.use_cases.get_grape_farming import GetGrapeFarming


class TestGetGrapeFarming:

    def test_successful_grape_farming_retrieval(self):
        mock_repo = Mock(spec=GrapeFarmingRepository)
        mock_repo.get_year_limits.return_value = (2000, 2025)
        mock_repo.get_grape_farming_by_year.return_value = [
            Grape(item="Grape Item", quantity=1000, classification="Class A")
        ]
        use_case = GetGrapeFarming(mock_repo)
        result = use_case.execute(2022)
        assert result == [Grape(item="Grape Item", quantity=1000, classification="Class A")]

    def test_grape_farming_year_required(self):
        mock_repo = Mock(spec=GrapeFarmingRepository)
        use_case = GetGrapeFarming(mock_repo)
        with pytest.raises(Exception) as exc_info:
            use_case.execute(None)
        assert str(exc_info.value) == "Year is required"

    def test_grape_farming_invalid_year(self):
        mock_repo = Mock(spec=GrapeFarmingRepository)
        use_case = GetGrapeFarming(mock_repo)
        with pytest.raises(Exception) as exc_info:
            use_case.execute(3000)
        assert str(exc_info.value) == "Invalid year"

    def test_grape_farming_year_out_of_range(self):
        mock_repo = Mock(spec=GrapeFarmingRepository)
        mock_repo.get_year_limits.return_value = (2000, 2025)
        use_case = GetGrapeFarming(mock_repo)
        result = use_case.execute(1999)
        assert result == []

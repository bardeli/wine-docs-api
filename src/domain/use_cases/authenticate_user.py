from datetime import datetime, timedelta, timezone

from jose import jwt
from passlib.context import CryptContext


from src.domain.repositories.user_repository import UserRepository


class AuthenticateUser:
    pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
    SECRET_KEY = "aa7104d38944b55e62da01b9987ad8008eb07b7bffeebd8db25ed861b4c24a4b"
    ALGORITHM = "HS256"
    ACCESS_TOKEN_EXPIRE_MINUTES = 30

    def __init__(self, user_repository: UserRepository):
        self.user_repository = user_repository

    def execute(self, username: str, password: str):
        user = self.user_repository.get_user(username)
        if not user:
            return False
        if user.disabled:
            return False
        if not self._verify_password(password, user.hashed_password):
            return False

        access_token_expires = timedelta(minutes=self.ACCESS_TOKEN_EXPIRE_MINUTES)
        access_token = self._create_access_token(data={"sub": user.username}, expires_delta=access_token_expires)
        return access_token

    def _verify_password(self, plain_password, hashed_password):
        return self.pwd_context.verify(plain_password, hashed_password)

    def _create_access_token(self, data: dict, expires_delta: timedelta | None = None):
        to_encode = data.copy()
        if expires_delta:
            expire = datetime.now(timezone.utc) + expires_delta
        else:
            expire = datetime.now(timezone.utc) + timedelta(minutes=15)
        to_encode.update({"exp": expire})
        encoded_jwt = jwt.encode(to_encode, self.SECRET_KEY, algorithm=self.ALGORITHM)
        return encoded_jwt

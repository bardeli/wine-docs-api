from unittest.mock import Mock

import pytest

from src.domain.entities.product import Product
from src.domain.repositories.wine_sales_repository import WineSalesRepository
from src.domain.use_cases.get_sales import GetSales


class TestGetSales:

    def test_successful_sales_retrieval(self):
        mock_repo = Mock(spec=WineSalesRepository)
        mock_repo.get_year_limits.return_value = (2000, 2025)
        mock_repo.get_sales.return_value = [Product(item="Product 1", subitem=None, quantity=1000)]
        use_case = GetSales(mock_repo)
        result = use_case.execute(2022)
        assert result == [Product(item="Product 1", subitem=None, quantity=1000)]

    def test_sales_year_required(self):
        mock_repo = Mock(spec=WineSalesRepository)
        use_case = GetSales(mock_repo)
        with pytest.raises(Exception) as exc_info:
            use_case.execute(None)
        assert str(exc_info.value) == "Year is required"

    def test_sales_invalid_year(self):
        mock_repo = Mock(spec=WineSalesRepository)
        use_case = GetSales(mock_repo)
        with pytest.raises(Exception) as exc_info:
            use_case.execute(3000)
        assert str(exc_info.value) == "Invalid year"

    def test_sales_year_out_of_range(self):
        mock_repo = Mock(spec=WineSalesRepository)
        mock_repo.get_year_limits.return_value = (2000, 2025)
        use_case = GetSales(mock_repo)
        result = use_case.execute(1999)
        assert result == []

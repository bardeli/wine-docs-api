from datetime import datetime

from src.domain.entities.product import Product
from src.domain.repositories.wine_sales_repository import WineSalesRepository


class GetSales:
    def __init__(self, sales_repo: WineSalesRepository):
        self.sales_repo = sales_repo

    def execute(self, year: int) -> list[Product]:
        if not year:
            raise Exception("Year is required")

        current_year = datetime.now().year

        if int(year) > current_year:
            raise Exception("Invalid year")

        start, end = self.sales_repo.get_year_limits()

        if year < start or year > end:
            return []

        return self.sales_repo.get_sales(year)

from datetime import datetime

from src.domain.entities.product import Product
from src.domain.repositories.wine_production_repository import WineProductionRepository


class GetProduction:
    def __init__(self, production_repo: WineProductionRepository):
        self.production_repo = production_repo

    def execute(self, year: int) -> list[Product]:
        if not year:
            raise Exception("Year is required")

        current_year = datetime.now().year

        if int(year) > current_year:
            raise Exception("Invalid year")

        start, end = self.production_repo.get_year_limits()

        if year < start or year > end:
            return []

        return self.production_repo.get_production(year)

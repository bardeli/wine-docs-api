from enum import Enum

from src.domain.entities.trade import Trade
from src.domain.repositories.trades_repository import GrapeAndProductTradesRepository


class TradeType(Enum):
    EXPORT = "EXPORT"
    IMPORT = "IMPORT"


class GetTradesInformation:
    def __init__(self, trade_repository: GrapeAndProductTradesRepository):
        self.trade_repository = trade_repository

    def execute(self, trade_type: TradeType, year: int) -> list[Trade]:

        switcher = {
            TradeType.EXPORT: self.trade_repository.get_exported_grape_derived_products,
            TradeType.IMPORT: self.trade_repository.get_imported_grape_derived_products,
        }

        # Get the function from switcher dictionary with the type as key (default to lambda: None if not found)
        func = switcher.get(trade_type, lambda y: None)

        # Execute the function
        return func(year)

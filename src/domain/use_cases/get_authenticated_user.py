import logging

from jose import jwt, JWTError

from src.domain.entities.token import TokenData
from src.domain.entities.user import User
from src.domain.repositories.user_repository import UserRepository


class GetAuthenticatedUser:
    SECRET_KEY = "aa7104d38944b55e62da01b9987ad8008eb07b7bffeebd8db25ed861b4c24a4b"
    ALGORITHM = "HS256"

    def __init__(self, user_repository: UserRepository):
        self.user_repository = user_repository

    def execute(self, token: str) -> User | None:
        try:
            payload = jwt.decode(token, self.SECRET_KEY, algorithms=[self.ALGORITHM])
            username: str = payload.get("sub")
            if username is None:
                logging.error("Invalid token data")
                return None
            token_data = TokenData(username=username)
        except JWTError:
            logging.error("Invalid token", exc_info=True)
            return None
        user = self.user_repository.get_user(username=token_data.username)
        if user is None:
            logging.error("User not found")
            return None

        if user.disabled:
            logging.error("User disabled")
            return None

        return user

from unittest.mock import patch

import pytest

from src.infrastructure.repositories.grape_scrap_repository import GrapeFarmingScrapRepository


class MockResponse:
    def __init__(self, status_code, text):
        self.status_code = status_code
        self.text = text


class TestGrapeFarmingScrapRepository:
    @pytest.fixture
    def repository(self):
        return GrapeFarmingScrapRepository()

    @patch("requests.get")
    def test_grape_farming_by_year_returns_grapes_when_response_is_successful(self, mock_get, repository):
        mock_get.side_effect = [
            MockResponse(
                status_code=200,
                text='<html><body><table class="tb_base tb_dados">'
                "<tr>"
                "<td class='tb_item'>TINTAS</td><td>800</td>"
                "</tr>"
                "<tr>"
                "<td class='tb_subitem'>Malbec</td><td>1000</td>"
                "</tr></table></body></html>",
            ),
            MockResponse(
                status_code=200,
                text='<html><body><table class="tb_base tb_dados">'
                "<tr>"
                "<td class='tb_item'>Sem classificação</td><td>700</td>"
                "</tr></table></body></html>",
            ),
            MockResponse(status_code=200, text=""),
            MockResponse(status_code=200, text=""),
        ]
        result = list(repository.get_grape_farming_by_year(2020))
        assert len(result) == 2
        assert any(grape.subitem == "Malbec" for grape in result)
        assert any(grape.quantity == 1000 for grape in result)
        assert any(grape.item == "TINTAS" for grape in result)
        assert any(grape.item == "Sem classificação" and grape.subitem is None for grape in result)

    @patch("requests.get")
    def test_grape_farming_by_year_raises_exception_when_response_is_not_successful(self, mock_get, repository):
        mock_get.return_value.status_code = 500
        with pytest.raises(Exception):
            list(repository.get_grape_farming_by_year(2020))

    @patch("requests.get")
    def test_year_limits_returns_years_when_response_is_successful(self, mock_get, repository):
        mock_get.return_value.status_code = 200
        mock_get.return_value.text = '<html><body><label class="lbl_pesq">[2000-2020]</label></body></html>'
        result = repository.get_year_limits()
        assert result == (2000, 2020)

    @patch("requests.get")
    def test_year_limits_raises_exception_when_response_is_not_successful(self, mock_get, repository):
        mock_get.return_value.status_code = 500
        with pytest.raises(Exception):
            repository.get_year_limits()

    @patch("requests.get")
    def test_year_limits_raises_exception_when_year_limits_are_not_found(self, mock_get, repository):
        mock_get.return_value.status_code = 200
        mock_get.return_value.text = '<html><body><label class="lbl_pesq">No years found</label></body></html>'
        with pytest.raises(Exception):
            repository.get_year_limits()

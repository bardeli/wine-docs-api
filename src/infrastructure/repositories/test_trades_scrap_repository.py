from unittest.mock import patch

import pytest

from src.infrastructure.repositories.trades_scrap_repository import TradesScrapRepository


class MockResponse:
    def __init__(self, status_code, text):
        self.status_code = status_code
        self.text = text


class TestTradesScrapRepository:
    @pytest.fixture
    def repository(self):
        return TradesScrapRepository()

    @patch("requests.get")
    def test_exported_products_return_trades_when_response_is_successful(self, mock_get, repository):
        mock_get.side_effect = [
            MockResponse(
                status_code=200,
                text='<html><body><table class="tb_base tb_dados">'
                "<tr><td>Japan</td><td>1000</td><td>1000</td></tr></table></body></html>",
            ),
            MockResponse(
                status_code=200,
                text='<html><body><table class="tb_base tb_dados">'
                "<tr><td>Japan</td><td>500</td><td>500</td></tr></table></body></html>",
            ),
            MockResponse(status_code=200, text=""),
            MockResponse(status_code=200, text=""),
        ]
        result = list(repository.get_exported_grape_derived_products(2020))
        assert len(result) == 2
        assert any(trade.country == "Japan" for trade in result)
        assert any(trade.quantity == 1000 for trade in result)
        assert any(trade.value == 1000 for trade in result)

    @patch("requests.get")
    def test_exported_products_raises_exception_when_response_is_not_successful(self, mock_get, repository):
        mock_get.return_value.status_code = 500
        with pytest.raises(Exception):
            list(repository.get_exported_grape_derived_products(2020))

    @patch("requests.get")
    def test_imported_products_return_trades_when_response_is_successful(self, mock_get, repository):
        mock_get.side_effect = [
            MockResponse(
                status_code=200,
                text='<html><body><table class="tb_base tb_dados">'
                "<tr><td>Japan</td><td>1000</td><td>1000</td></tr></table></body></html>",
            ),
            MockResponse(
                status_code=200,
                text='<html><body><table class="tb_base tb_dados">'
                "<tr><td>Iraque</td><td>500</td><td>502</td></tr></table></body></html>",
            ),
            MockResponse(status_code=200, text=""),
            MockResponse(status_code=200, text=""),
            MockResponse(status_code=200, text=""),
        ]
        result = list(repository.get_imported_grape_derived_products(2020))
        assert len(result) == 2
        assert any(trade.country == "Iraque" for trade in result)
        assert any(trade.quantity == 500 for trade in result)
        assert any(trade.value == 502 for trade in result)

    @patch("requests.get")
    def test_imported_products_raises_exception_when_response_is_not_successful(self, mock_get, repository):
        mock_get.return_value.status_code = 500
        with pytest.raises(Exception):
            list(repository.get_imported_grape_derived_products("2020"))

    @patch("requests.get")
    def test_year_limits_returns_years_when_response_is_successful(self, mock_get, repository):
        mock_get.return_value.status_code = 200
        mock_get.return_value.text = '<html><body><label class="lbl_pesq">[2000-2020]</label></body></html>'
        result = repository.get_year_limits()
        assert result == (2000, 2020)

    @patch("requests.get")
    def test_year_limits_raises_exception_when_response_is_not_successful(self, mock_get, repository):
        mock_get.return_value.status_code = 500
        with pytest.raises(Exception):
            repository.get_year_limits()

    @patch("requests.get")
    def test_year_limits_raises_exception_when_year_limits_are_not_found(self, mock_get, repository):
        mock_get.return_value.status_code = 200
        mock_get.return_value.text = '<html><body><label class="lbl_pesq">No years found</label></body></html>'
        with pytest.raises(Exception):
            repository.get_year_limits()

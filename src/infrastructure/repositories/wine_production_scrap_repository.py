import logging
import re
from typing import List

import requests
from bs4 import BeautifulSoup

from src.domain.entities.product import Product
from src.domain.repositories.wine_production_repository import WineProductionRepository


class WineProductionScrapRepository(WineProductionRepository):
    def get_production(self, year: str) -> List[Product]:
        response = requests.get(f"http://vitibrasil.cnpuv.embrapa.br/index.php?ano={year}&opcao=opt_02")

        if response.status_code != 200:
            raise Exception(f"Failed to fetch data. Status code: {response.status_code}")

        soup = BeautifulSoup(response.text, "html.parser")

        table = soup.find("table", {"class": "tb_base tb_dados"})
        rows = table.find_all("tr")

        products = []
        current_item = None

        for row in rows:
            cells = row.find_all("td")
            if not cells:
                continue

            if "tb_item" in cells[0].get("class", []):
                current_item = cells[0].text.strip()
            elif "tb_subitem" in cells[0].get("class", []):
                subitem = cells[0].text.strip()
                quantity = None if cells[1].text.strip() == "-" else float(cells[1].text.strip().replace(".", ""))
                products.append(Product(item=current_item, subitem=subitem, quantity=quantity))

        return products

    def get_year_limits(self) -> tuple[int, int]:
        response = requests.get("http://vitibrasil.cnpuv.embrapa.br/index.php?opcao=opt_02")

        if response.status_code != 200:
            raise Exception(f"Failed to fetch data. Status code: {response.status_code}")

        soup = BeautifulSoup(response.text, "html.parser")

        yearLabel = soup.find("label", {"class": "lbl_pesq"})

        options = self._extract_years(yearLabel.text)

        return options

    def _extract_years(self, label) -> tuple[int, int]:
        match = re.search(r"\[(\d+)-(\d+)]", label)

        if match:
            start_year, end_year = match.groups()
            logging.info(f"Start Year: {start_year}, End Year: {end_year}")
            return int(start_year), int(end_year)

        raise Exception("Failed to extract year limits")

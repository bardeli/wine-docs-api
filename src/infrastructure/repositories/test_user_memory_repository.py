from unittest.mock import patch

from src.domain.entities.user import AuthUser
from src.infrastructure.repositories.user_memory_repository import UserMemoryRepository


def test_get_user_returns_auth_user_when_username_exists():
    repository = UserMemoryRepository()
    user = repository.get_user("systemadmin")
    assert isinstance(user, AuthUser)
    assert user.username == "systemadmin"


def test_get_user_returns_none_when_username_does_not_exist():
    repository = UserMemoryRepository()
    user = repository.get_user("nonexistentuser")
    assert user is None


@patch.dict(
    "src.infrastructure.repositories.user_memory_repository.UserMemoryRepository.fake_users_db",
    {
        "newuser": {
            "username": "newuser",
            "full_name": "New User",
            "email": "new@mlet.com",
            "hashed_password": "$2b$12$EixZaYVK1fsbw1ZfbX3OXePaWxn96p36WQoeG6Lruj3vjPGga31lW",
            "disabled": False,
        }
    },
)
def test_get_user_returns_newly_added_user():
    repository = UserMemoryRepository()
    user = repository.get_user("newuser")
    assert isinstance(user, AuthUser)
    assert user.username == "newuser"

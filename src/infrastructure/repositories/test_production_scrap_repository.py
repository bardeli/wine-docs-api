import unittest
from unittest.mock import patch, Mock

from src.domain.entities.product import Product
from src.infrastructure.repositories.wine_production_scrap_repository import (
    WineProductionScrapRepository,
)


class TestWineProductionScrapRepository(unittest.TestCase):

    @patch("requests.get")
    def test_get_production(self, mock_get):
        # Mock the response from requests.get
        mock_response = Mock()
        mock_response.status_code = 200
        mock_response.text = (
            "<html>"
            '<table class="tb_base tb_dados">'
            "<tr>"
            '<td class="tb_item">item1</td>'
            "</tr>"
            "<tr>"
            '<td class="tb_subitem">subitem1</td>'
            '<td class="tb_subitem">1</td>'
            "</tr>"
            "</table>"
            "</html>"
        )
        mock_get.return_value = mock_response

        repo = WineProductionScrapRepository()
        result = repo.get_production("2022")

        self.assertEqual(result, [Product(item="item1", subitem="subitem1", quantity=1.0)])

    @patch("requests.get")
    def test_get_year_limits(self, mock_get):
        # Mock the response from requests.get
        mock_response = Mock()
        mock_response.status_code = 200
        mock_response.text = (
            "<html>"
            "<label class= lbl_pesq>[2000-2022]</label>"
            '<table class="tb_base tb_dados">'
            "<tr>"
            '<td class="tb_item">item1</td>'
            "</tr>"
            "<tr>"
            '<td class="tb_subitem">subitem1</td>'
            '<td class="tb_subitem">1</td>'
            "</tr>"
            "</table>"
            "</html>"
        )
        mock_get.return_value = mock_response

        repo = WineProductionScrapRepository()
        result = repo.get_year_limits()

        self.assertEqual(result, (2000, 2022))

    @patch("requests.get")
    def test_get_production_raises_exception_when_status_code_not_200(self, mock_get):
        # Mock the response from requests.get
        mock_response = Mock()
        mock_response.status_code = 404
        mock_get.return_value = mock_response

        repo = WineProductionScrapRepository()
        with self.assertRaises(Exception):
            repo.get_production("2022")

    @patch("requests.get")
    def test_get_year_limits_raises_exception_when_status_code_not_200(self, mock_get):
        # Mock the response from requests.get
        mock_response = Mock()
        mock_response.status_code = 404
        mock_get.return_value = mock_response

        repo = WineProductionScrapRepository()
        with self.assertRaises(Exception):
            repo.get_year_limits()


if __name__ == "__main__":
    unittest.main()

from src.domain.entities.user import AuthUser
from src.domain.repositories.user_repository import UserRepository


class UserMemoryRepository(UserRepository):
    fake_users_db = {
        "systemadmin": {
            "username": "systemadmin",
            "full_name": "System Admin",
            "email": "system@mlet.com",
            "hashed_password": "$2b$12$EixZaYVK1fsbw1ZfbX3OXePaWxn96p36WQoeG6Lruj3vjPGga31lW",
            "disabled": False,
        },
        "app_service": {
            "username": "app_service",
            "full_name": "App Service",
            "email": "appe@mlet.com",
            "hashed_password": "$2b$12$EixZaYVK1fsbw1ZfbX3OXePaWxn96p36WQoeG6Lruj3vjPGga31lW",
            "disabled": True,
        },
    }

    def get_user(self, username: str) -> AuthUser | None:
        if username in self.fake_users_db:
            user_dict = self.fake_users_db[username]
            return AuthUser(**user_dict)
        return None

import concurrent
import logging
import re
from concurrent.futures import ThreadPoolExecutor
from enum import Enum
from typing import List

import requests
from bs4 import BeautifulSoup

from src.domain.entities.trade import Trade
from src.domain.repositories.trades_repository import GrapeAndProductTradesRepository


class TradeType(Enum):
    EXPORTATION = "opt_06"
    IMPORTATION = "opt_05"


class TradesScrapRepository(GrapeAndProductTradesRepository):

    importation_products = {
        "Wine": "subopt_01",
        "Sparkling Wine": "subopt_02",
        "Fresh Grapes": "subopt_03",
        "raisin": "subopt_04",
        "Grape Juice": "subopt_05",
    }

    exportation_products = {
        "Wine": "subopt_01",
        "Sparkling Wine": "subopt_02",
        "Fresh Grapes": "subopt_03",
        "Grape Juice": "subopt_04",
    }

    def get_exported_grape_derived_products(self, year: int) -> List[Trade]:
        with ThreadPoolExecutor(max_workers=4) as executor:
            futures = {
                executor.submit(
                    self._get_trades_importation,
                    year,
                    classification,
                    TradeType.EXPORTATION,
                ): classification
                for classification in self.exportation_products.items()
            }
            for future in concurrent.futures.as_completed(futures):
                yield from future.result()

    def get_imported_grape_derived_products(self, year: int) -> List[Trade]:
        with ThreadPoolExecutor(max_workers=5) as executor:
            futures = {
                executor.submit(
                    self._get_trades_importation,
                    year,
                    classification,
                    TradeType.IMPORTATION,
                ): classification
                for classification in self.importation_products.items()
            }
            for future in concurrent.futures.as_completed(futures):
                yield from future.result()

    def _get_trades_importation(self, year: int, classification, trade_type: TradeType) -> List[Trade]:
        response = requests.get(
            f"http://vitibrasil.cnpuv.embrapa.br/index.php?ano={year}&opcao={trade_type.value}"
            f"&subopcao={classification[1]}"
        )

        if response.status_code != 200:
            raise Exception(f"Failed to fetch data. Status code: {response.status_code}")

        soup = BeautifulSoup(response.text, "html.parser")

        table = soup.find("table", {"class": "tb_base tb_dados"})

        if (table is None) or (table.find_all("tr") is None):
            return []

        rows = table.find_all("tr")

        trades = []

        for row in rows:
            cells = row.find_all("td")
            if not cells:
                continue

            country = cells[0].text.strip()

            if country == "Total":
                continue

            quantity = (
                None
                if cells[1].text.strip() == "*" or cells[1].text.strip() == "-"
                else float(cells[1].text.strip().replace(".", ""))
            )
            value = (
                None
                if cells[2].text.strip() == "*" or cells[2].text.strip() == "-"
                else float(cells[2].text.strip().replace(".", ""))
            )

            trades.append(Trade(country=country, quantity=quantity, value=value, product=classification[0]))

        return trades

    def get_year_limits(self) -> tuple[int, int]:
        response = requests.get("http://vitibrasil.cnpuv.embrapa.br/index.php?opcao=opt_05")

        if response.status_code != 200:
            raise Exception(f"Failed to fetch data. Status code: {response.status_code}")

        soup = BeautifulSoup(response.text, "html.parser")

        yearLabel = soup.find("label", {"class": "lbl_pesq"})

        options = self._extract_years(yearLabel.text)

        return options

    def _extract_years(self, label) -> tuple[int, int]:
        match = re.search(r"\[(\d+)-(\d+)]", label)

        if match:
            start_year, end_year = match.groups()
            logging.info(f"Start Year: {start_year}, End Year: {end_year}")
            return int(start_year), int(end_year)

        raise Exception("Failed to extract year limits")

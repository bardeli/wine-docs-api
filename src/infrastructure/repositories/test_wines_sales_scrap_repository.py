from unittest.mock import patch

import pytest

from src.infrastructure.repositories.wine_sales_scrap_repository import WineSalesScrapRepository


class TestWineSalesScrapRepository:
    @pytest.fixture
    def repository(self):
        return WineSalesScrapRepository()

    @patch("requests.get")
    def test_get_sales_returns_products_when_response_is_successful(self, mock_get, repository):
        mock_get.return_value.status_code = 200
        mock_get.return_value.text = (
            '<html><body><table class="tb_base tb_dados">'
            '<tr><td class="tb_item">Item 1</td><td>1000</td></tr></table></body></html>'
        )
        result = repository.get_sales("2020")
        assert len(result) == 1
        assert result[0].item == "Item 1"
        assert result[0].quantity == 1000

    @patch("requests.get")
    def test_get_sales_raises_exception_when_response_is_not_successful(self, mock_get, repository):
        mock_get.return_value.status_code = 500
        with pytest.raises(Exception):
            repository.get_sales("2020")

    @patch("requests.get")
    def test_get_year_limits_returns_years_when_response_is_successful(self, mock_get, repository):
        mock_get.return_value.status_code = 200
        mock_get.return_value.text = '<html><body><label class="lbl_pesq">[2000-2020]</label></body></html>'
        result = repository.get_year_limits()
        assert result == (2000, 2020)

    @patch("requests.get")
    def test_get_year_limits_raises_exception_when_response_is_not_successful(self, mock_get, repository):
        mock_get.return_value.status_code = 500
        with pytest.raises(Exception):
            repository.get_year_limits()

    @patch("requests.get")
    def test_get_year_limits_raises_exception_when_year_limits_are_not_found(self, mock_get, repository):
        mock_get.return_value.status_code = 200
        mock_get.return_value.text = '<html><body><label class="lbl_pesq">No years found</label></body></html>'
        with pytest.raises(Exception):
            repository.get_year_limits()

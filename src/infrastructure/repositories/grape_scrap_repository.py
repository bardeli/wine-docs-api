import concurrent
import logging
import re
from concurrent.futures import ThreadPoolExecutor
from typing import List

import requests
from bs4 import BeautifulSoup

from src.domain.entities.grape import Grape
from src.domain.repositories.grape_farming_repository import GrapeFarmingRepository


class GrapeFarmingScrapRepository(GrapeFarmingRepository):

    wine_classifications = {
        "Wine": "subopt_01",
        "American and Hybrid": "subopt_02",
        "Table Grape": "subopt_03",
        "Unclassified": "subopt_04",
    }

    def get_grape_farming_by_year(self, year: int) -> List[Grape]:
        with ThreadPoolExecutor(max_workers=4) as executor:
            futures = {
                executor.submit(
                    self._get_grape_farming_by_year_and_classification,
                    year,
                    classification,
                ): classification
                for classification in self.wine_classifications.items()
            }
            for future in concurrent.futures.as_completed(futures):
                yield from future.result()

    def _get_grape_farming_by_year_and_classification(self, year: int, classification) -> List[Grape]:
        response = requests.get(
            f"http://vitibrasil.cnpuv.embrapa.br/index.php?ano={year}&opcao=opt_03&subopcao={classification[1]}"
        )

        if response.status_code != 200:
            raise Exception(f"Failed to fetch data. Status code: {response.status_code}")

        soup = BeautifulSoup(response.text, "html.parser")

        table = soup.find("table", {"class": "tb_base tb_dados"})

        if table is None:
            return []

        rows = table.find_all("tr")

        products = []
        current_item = None

        for row in rows:
            cells = row.find_all("td")
            if not cells:
                continue

            if "tb_item" in cells[0].get("class", []):
                current_item = cells[0].text.strip()
                quantity = (
                    None
                    if cells[1].text.strip() == "*" or cells[1].text.strip() == "-"
                    else float(cells[1].text.strip().replace(".", ""))
                )
                products.append(
                    Grape(
                        item=current_item,
                        subitem=None,
                        quantity=quantity,
                        classification=classification[0],
                    )
                )
            elif "tb_subitem" in cells[0].get("class", []):
                if products[-1].subitem is None:
                    products.pop()

                subitem = cells[0].text.strip()
                quantity = (
                    None
                    if cells[1].text.strip() == "*" or cells[1].text.strip() == "-"
                    else float(cells[1].text.strip().replace(".", ""))
                )
                products.append(
                    Grape(
                        item=current_item,
                        subitem=subitem,
                        quantity=quantity,
                        classification=classification[0],
                    )
                )

        return products

    def get_year_limits(self) -> tuple[int, int]:
        response = requests.get("http://vitibrasil.cnpuv.embrapa.br/index.php?opcao=opt_03")

        if response.status_code != 200:
            raise Exception(f"Failed to fetch data. Status code: {response.status_code}")

        soup = BeautifulSoup(response.text, "html.parser")

        yearLabel = soup.find("label", {"class": "lbl_pesq"})

        options = self._extract_years(yearLabel.text)

        return options

    def _extract_years(self, label) -> tuple[int, int]:
        match = re.search(r"\[(\d+)-(\d+)]", label)

        if match:
            start_year, end_year = match.groups()
            logging.info(f"Start Year: {start_year}, End Year: {end_year}")
            return int(start_year), int(end_year)

        raise Exception("Failed to extract year limits")

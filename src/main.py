import logging

from fastapi import FastAPI

from src.application.api.authentication_router import router as auth_router
from src.application.api.grapes_router import router as grapes_router
from src.application.api.products_router import router as production_router
from src.application.api.trades_router import router as trade_router

app = FastAPI()
app.include_router(production_router)
app.include_router(auth_router)
app.include_router(grapes_router)
app.include_router(trade_router)

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(module)s:%(lineno)d %(funcName)s - %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)

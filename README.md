# Wine Docs Api

***

## Description

Get information about wine production and commercialization from Brazil to the world.

## Installation

project uses pipenv to manage dependencies. You can install all the required packages using the following command in
your terminal:

```bash
make install
```

## Usage

To run the project, you can use the following command:

```bash
pipenv run uvicorn src.application.api.main:app --reload
```

The --reload flag enables hot reloading, which means the server will automatically update whenever you make changes to
your code
If you prefer to use Docker, you have a Makefile with a docker-run command that builds a Docker image and runs it:

```bash
make docker-run
```

## Testing

To run the tests, you can use the following command:

```bash
make test
```

```bash
make test-html
```

This command will run the tests and generate a coverage report.

## Lint

To run linter

```bash
make lint
```

## Format

```bash
make format
```

## API Documentation

The API documentation is available at http://localhost:8000/docs

# Clean Architecture

## Introduction

Clean Architecture is a software design philosophy that separates the software into layers, with each layer having a
specific role and responsibility. The main goal of Clean Architecture is to make the system easy to understand, develop,
test, and maintain.

## Principles

Clean Architecture is based on several principles:

1. **Separation of Concerns**: Each layer in the system has a specific role and responsibility. This makes the system
   easier to understand and maintain.

2. **Dependency Rule**: Dependencies between the layers only point inwards. The outer layers can depend on the inner
   layers, but not the other way around.

3. **Independent of Frameworks**: The architecture does not depend on specific technology, database, or web frameworks.

4. **Testable**: The business rules can be tested without the UI, database, or any external element.

## Layers

Clean Architecture typically consists of the following layers:

1. **Entities**: These are the business objects of the application.

2. **Use Cases**: This layer contains specific business rules for the application.

3. **Interface Adapters**: This layer converts data from the format most convenient for the use cases and entities, to
   the format most convenient for things like the web, database, UI, etc.

4. **Frameworks and Drivers**: This is the outermost layer which includes frameworks and tools such as the database and
   the web framework.

## Benefits

Implementing Clean Architecture can provide several benefits:

1. **Independent of UI**: The UI can change without changing the rest of the system. A web UI could be replaced with a
   console UI, for example.

2. **Independent of Database**: The database can change without changing the rest of the system.

3. **Independent of any external agency**: In fact, the business rules simply don’t know anything at all about the
   outside world.

4. **Testable**: The business rules can be tested without the UI, Database, Web Server, or any external element.

 ```mermaid 
graph TD
    A[FastAPI] --> B[Use Cases]
    B --> C[Entities]
    B --> D[Repositories]
    D --> E[Data Source]
```